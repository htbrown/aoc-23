from io import StringIO

def a(file):
    return None

def b(file):
    return None

test = """"""

#with StringIO(test) as file:
with open("resources/1.txt") as file:
    part_a = a(file)
    file.seek(0)
    part_b = b(file)

    print("a:", str(part_a))
    print("b:", str(part_b))
