from io import StringIO


def a(file):
    quantities = {
        "red": 12,
        "green": 13,
        "blue": 14
    }
    sum = 0
    for line in file:
        id = int(line.split(":")[0].split(" ")[1])
        given = [i.strip() for i in line.strip().split(":")[1].split(";")]

        valid = True
        for blocks in given:
            decoded = [i.strip().split(" ") for i in blocks.split(",")]
            for i in decoded:
                if quantities[i[1]] < int(i[0]):
                    valid = False
                    break
        if valid:
            sum += id

    return sum


def b(file):
    sum = 0
    for line in file:
        given = [i.strip() for i in line.strip().split(":")[1].split(";")]
        maximum = {
            "red": 0,
            "green": 0,
            "blue": 0
        }
        for blocks in given:
            decoded = [i.strip().split(" ") for i in blocks.split(",")]
            for i in decoded:
                if maximum[i[1]] < int(i[0]):
                    maximum[i[1]] = int(i[0])
        sum += maximum["red"] * maximum["green"] * maximum["blue"]
    return sum


test = """Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"""

# with StringIO(test) as file:
with open("resources/2.txt") as file:
    part_a = a(file)
    file.seek(0)
    part_b = b(file)

    print("a:", str(part_a))
    print("b:", str(part_b))
