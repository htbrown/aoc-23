from io import StringIO


def a(file):
    sum = 0
    grid = [[j for j in i.strip()] for i in file]

    buffer = ""
    include = False
    for row in range(len(grid)):
        for col in range(len(grid[row])):
            if grid[row][col].isnumeric():
                buffer += grid[row][col]
                adj = [grid[row + dr][col + dc] for dr, dc in [(0, 1), (1, 0), (0, -1), (-1, 0), (1, 1), (-1, -1), (1, -1), (-1, 1)] if 0 <= row + dr < len(grid) and 0 <= col + dc < len(grid[row])]
                if any([i != "." and not i.isalnum() for i in adj]):
                    include = True
            elif not grid[row][col].isnumeric() and buffer != "" and include:
                sum += int(buffer)
                buffer = ""
                include = False
            else:
                buffer = ""
                include = False
    return sum


def b(file):
    sum = 0
    grid = [[j for j in i.strip()] for i in file]
    return grid


test = """467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
"""

# with StringIO(test) as file:
with open("resources/3.txt") as file:
    part_a = a(file)
    file.seek(0)
    part_b = b(file)

    print("a:", str(part_a))
    print("b:", str(part_b))
