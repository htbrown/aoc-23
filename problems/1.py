from io import StringIO


def a(file):
    sum = 0
    for line in file:
        nums = [int(i) for i in line.strip() if i.isnumeric()]
        if len(nums) < 1:
            nums = [0]
        sum += nums[0]*10 + nums[-1]
    return sum


def b(file):
    sum = 0
    text = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
    for line in file:
        buffer = ""
        nums = []
        for char in line.strip():
            buffer += char
            bools = [buffer.endswith(i) for i in text]
            if char.isnumeric():
                nums.append(int(char))
                buffer = ""
            elif any(bools):
                nums.append(bools.index(True) + 1)
                buffer = char
        sum += nums[0]*10 + nums[-1]
    return sum


test = """two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen"""

# with StringIO(test) as file:
with open("resources/1.txt") as file:
    part_a = a(file)
    file.seek(0)
    part_b = b(file)

    print("a:", str(part_a))
    print("b:", str(part_b))
