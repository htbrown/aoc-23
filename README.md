# Advent of Code 2023

> ⚠️ **This repository contains spoilers for Advent of Code 2023.**

These are my (late) attempts at [Advent of Code](https://adventofcode.com) 2023. They are probably not good, so please don't get angry with me.

This repository is intended to show my attempt at Advent of Code 2023 and should not be used to complete the challenge yourself (if anyone was even thinking of doing so in the first place). It is much more rewarding if you work out the solutions on your own.

Also, my solutions are likely to be far from optimal and might not even function properly. I don't know how to Advent of Code properly, so looking at my solutions if you're in genuine need of help is probably not going to be useful. Have a chat to someone on the [Advent of Code subreddit](https://www.reddit.com/r/adventofcode/).
